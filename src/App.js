import './App.css';
import Header from './component/Header';
import { Routes, Route } from 'react-router-dom';
import Home from './component/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import Detail from './component/Detail';



function App() {
  return (
    < >
        <Header/>
        <Routes>
          <Route path="/" element={<Home/>} />
          <Route path="/cocktail/:id" element={<Detail/>} />
         
         
        </Routes>
    </>
  );
}

export default App;
