import React, { useEffect, useReducer, useState } from 'react'
import {Card, Button, Image, Col, Row} from 'react-bootstrap'
import { useNavigate, useParams } from 'react-router-dom'
import ClipLoader from "react-spinners/ClipLoader";

import {api} from './../api'
import {initalState, reducer} from './../reducer'
import { ActionTypes } from '../reducer/ActionTypes';

function Detail() {
  const {id} = useParams();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);

  const [state,dispatch]= useReducer(reducer,initalState);

    const fetchdetailApi = async() => {
        const res = await api.get(`lookup.php?i=${id}`)
        if(res.data.drinks.length > 0){
          dispatch({
              type: ActionTypes.SELECT_DATA,
              payload: res.data.drinks[0]
          })
        }
        setLoading(false);
        
    }

    const backtoList = () => {
      navigate('/')
    }
  
  useEffect(()=>{
    fetchdetailApi();
  },[id])

  
  return (
    <div className='container'>
      {loading == false ? (<Row className='mt-4 mx-3'>
           <Col md={4}>
            <Image src={state.item.strDrinkThumb} fluid/>
           </Col>
           <Col>
              <h5>Name : {state.item.strDrink}</h5>
              <p className='fw-bold'>Drink Id : {state.item.idDrink}</p>
              <p>Tags : {state.item.strTags}</p>
              <p>Cateogry : {state.item.strCategory}</p>
              <p>Type : {state.item.strAlcoholic}</p>
              <p>Receipt: {state.item.strInstructions}</p>
              
           </Col>

           <Button className='btn btn-success mt-3' onClick={backtoList}>Back to List</Button>
      </Row>): <div className=' vh-100 d-flex align-items-center justify-content-center'>
        <ClipLoader
        color="#3498db"
        
        size={150}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
        </div>}
    </div>

  )
}

export default Detail
