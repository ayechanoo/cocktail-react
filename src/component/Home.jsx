import React, { useEffect, useReducer } from 'react'

import Cocktail  from './Cocktail';

function Home() {
    
  return (
    <div>
        <Cocktail />
    </div>
  )
}

export default Home
