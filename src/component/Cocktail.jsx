import {useEffect, useReducer, useState} from 'react'
import SampleCard from './SampleCard'
import {api} from './../api'
import {initalState, reducer} from './../reducer'
import { ActionTypes } from '../reducer/ActionTypes';
import {Col, Row, Card, Button} from 'react-bootstrap'
import ClipLoader from "react-spinners/ClipLoader";


function Cocktail() {

  const [state,dispatch]= useReducer(reducer,initalState);
  const [loading,setLoading ] = useState(true);

    const fetchapi = async() => {
      try{
        const res = await api.get('search.php?f=a');
        console.log(res.data.drinks);
        dispatch({
            type: ActionTypes.FETCH_DATA,
            payload: res.data.drinks
        })
        setLoading(false);
      }catch(err){
        
        alert('Something went wrong!');
      }
    }
    useEffect(() => {
        fetchapi()
    },[])
    
  return (
    <div className='container mt-3'>
        {loading == false ? (<Row gap='2'>
            

            {state.list.map(item => (
                 <Col sm md='4' lg="4" xl="3" className='mb-2 mt-2'>

                 <SampleCard blog={item} />
                     
                 </Col>
            ))}

            

            
        </Row>) :<div className=' vh-100 d-flex align-items-center justify-content-center'>
        <ClipLoader
        color="#3498db"
        
        size={150}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
        </div>
}
    </div>
  )
}

export default Cocktail
