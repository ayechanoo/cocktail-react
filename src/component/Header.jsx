import { NavLink } from "react-router-dom";
import {Navbar, Container,Nav} from 'react-bootstrap';

function Header() {
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand >BLISS</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            
          <NavLink
        to="/"
        className={({ isActive }) =>
            isActive ? "active nav-link" : "nav-link"
        }
        >
        Home
        </NavLink>

        
    

            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;