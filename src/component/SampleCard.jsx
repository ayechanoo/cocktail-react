import React from 'react'

import {Card, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'

function SampleCard({blog}) {

  return (
    <div>
      <Card style={{ width: '18rem' }}>
        <Card.Img variant="top" src={blog.strDrinkThumb} style={{maxHeight:'80'}}/>
        <Card.Body>
            <Card.Title>{blog.strDrink}</Card.Title>
            <Card.Text>
              {blog.strInstructions.substring(1,30)}...
            </Card.Text>
            <Link variant="primary" className='btn btn-primary' to={`/cocktail/${blog.idDrink}`}>Get Receipt!</Link>
        </Card.Body>
        </Card>
    </div>
  )
}

export default SampleCard
