import { ActionTypes } from "./ActionTypes";

export const initalState = {
    list: [],
    item: {}
}

export const reducer = (state, {type, payload}) => {
    switch (type) {
        case ActionTypes.FETCH_DATA:
            return {...state,list:payload};
            break;

        case ActionTypes.SELECT_DATA:
                return {...state, item:payload};
                break;
    
        default: return state;
            break;
    }
}